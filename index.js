(function(app) {

    app.component('tryCalculator', {
        templateUrl: 'tryCalculator.html',
        controller: function($element) {
            var vm = this;

            vm.$onInit = function() {
                vm.calculations = [];
                vm.reset();
            };

            vm.add = function() {
                vm.calculations.push({
                    num1: vm.num1,
                    num2: vm.num2,
                    sum: parseFloat(vm.num1) + parseFloat(vm.num2)
                });

                vm.reset();
            };

            vm.reset = function() {
                $element.find('input')[0].focus();
                vm.num1 = '';
                vm.num2 = '';
            };
        }
    });

})(angular.module('tryCalculator', []));